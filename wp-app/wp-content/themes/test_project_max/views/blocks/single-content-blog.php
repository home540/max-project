<?php
/*
    Title: Single content blog
    Description: Single content blog
    Category: Page Headers
    Icon: admin-comments
    Keywords: single-content-blog
    */
?>

<?php

?>

<?php


$image = get_field('image', get_the_ID());
$headline = get_field('headline', get_the_ID());
$subheadline = get_field('subheadline', get_the_ID());
$video = get_field('video', get_the_ID());
$date = get_field('post_date');
$date = date_create($date);
$criterias = get_the_terms(get_the_ID(), 'criteria_posts');
$discription = get_field('discription');
$quota = get_field('quota');
$video = get_field('video');
$image_content = get_field('image_content');
$discription_video = get_field('discription_video');

?>
    <div class="single-blog-background">
        <div class="single-blog-body">
            <div class="single-blog-headline">
                <?= $headline ?>
            </div>
            <div class="single-blog-pages">
                <ul class="breadcrumb">
                    <li><a href="http://localhost:8080/">Home .</a></li>
                    <li><a href="#">Pages .</a></li>
                    <li> <?= $headline ?></li>
                </ul>
            </div>
        </div>
    </div>
    <section>
        <div class="container">
            <div class="single-post-wrapper-head">

                <?php if (!empty($image)): ?>
                    <div class="single-post-image">
                        <img src="<?= $image['url'] ?>" alt="">
                    </div>
                <?php endif ?>

                <div class="single-post-wrapper-categories-date">

                    <?php if (!empty($criterias)): ?>
                        <div class="single-post-categories">
                            <?php foreach ($criterias as $criteria): ?>
                                <?= $criteria->name ?>
                            <?php endforeach; ?>
                        </div>
                    <?php endif ?>

                    <?php if (!empty($date)): ?>
                        <div class="single-post-date">
                            <?= date_format($date, 'j F, Y') ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <?php if (!empty($headline)): ?>
                <div class="single-post-headline">
                    <?= $headline ?>
                </div>
            <?php endif ?>

            <?php if (!empty($subheadline)): ?>
                <div class="single-post-subheadline">
                    <?= $subheadline ?>
                </div>
            <?php endif ?>

            <?php if (!empty($discription)): ?>
                <div class="single-post-discription">
                    <?= $discription ?>
                </div>
            <?php endif ?>

            <div class="single-post-quota-wrapper">


                <?php if (!empty($quota)): ?>
                    <div class="single-post-quota">
                        <?= $quota ?>
                    </div>
                <?php endif ?>
            </div>

            <div class="single-post-video-image-wrapper">
                <?php if (!empty($video)): ?>
                    <div class="single-post-video">
                        <?= $video ?>
                    </div>
                <?php endif ?>


                <?php if (!empty($image_content)): ?>
                    <div class="single-post-image-content">
                        <img src="<?= $image_content['url'] ?>" alt="">
                    </div>
                <?php endif ?>
            </div>

            <?php if (!empty($discription_video)):?>
            <div class="single-post-discription_video">
                <?=$discription_video?>
            </div>
            <?php endif?>

        </div>
    </section>

<?php if (!is_admin()) : ?>


<?php else: ?>
    Hero module
<?php endif; ?>