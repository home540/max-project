<?php
/*
    Title: Mestonix
    Description: Mestonix
    Category: Page Headers
    Icon: admin-comments
    Keywords: mestonix
    */
?>

<?php
$mestonix = get_field('mestonix');
?>

<?php if (!is_admin()) : ?>
    <section>
        <div class="container">
            <?php if (!empty($mestonix)) : ?>
                <div class="mestonix">
                    <img src="<?= $mestonix['url'] ?>" alt="">
                </div>
            <?php endif; ?>
        </div>
    </section>

<?php else: ?>
    Mestonix module
<?php endif; ?>