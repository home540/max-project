<?php
/*
    Title: Leatest Products module
    Description: Leatest Products module
    Category: Page Headers
    Icon: admin-comments
    Keywords: leatest-products-module
    */
?>

<?php
$headline = get_field('headline');

?>


<?php if (!is_admin()) : ?>

    <section>
        <div class="container">
            <?php if (!empty($headline)): ?>
                <div class="leatest-products-headline">
                    <?= $headline ?>
                </div>
            <?php endif; ?>
        </div>
    </section>

<?php else: ?>
    What Shopex Offer! module
<?php endif; ?>