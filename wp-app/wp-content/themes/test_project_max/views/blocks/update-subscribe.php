<?php
/*
    Title: Update subscribe
    Description: Update subscribe
    Category: Page Headers
    Icon: admin-comments
    Keywords: update-subscribe
    */
?>

<?php
$headline = get_field('headline');
$background_image = get_field('background_image');
$button = get_field('button');

?>

<?php if (!is_admin()) : ?>
    <section class="update-subscribe" style="background-image: url(<?= $background_image['url'] ?>)">
<div class="update-subscribe-block">
    <?php if (!empty($headline)) : ?>
        <h6 class="update-subscribe-headline">
            <?= $headline ?>
        </h6>
    <?php endif; ?>
    <?php if (!empty($button)) : ?>
        <div class="update-subscribe-button">
            <a href="<?= $button['url'] ?>">
                <?= $button['title'] ?>
            </a>
        </div>

    <?php endif; ?>
</div>

    </section>

<?php else: ?>
    Update subscribe module
<?php endif; ?>