<?php
/*
    Title: featured product module
    Description: featured product module
    Category: Page Headers
    Icon: admin-comments
    Keywords: featured-product-module
    */
?>

<?php
$headline = get_field('headline');
$select_featured_products = get_field('select_featured_products');
?>
    <section>
        <?php if (!empty($headline)) : ?>
            <div class="feature-headline">
                <?= $headline ?>
            </div>
        <?php endif; ?>
        <div class="feature-block">
            <?php foreach ($select_featured_products as $item) : ?>
                <?php
                $title = $item->post_title;
                $price = get_field('price', $item->ID);
                $Featured_image = get_field('image', $item->ID);
                $subheadline = get_field('subheadline', $item->ID);
                $code = get_field('code', $item->ID);
                $colors = get_field('colors_products', $item->ID);
                $button = get_field('button', $item->ID);

                ?>

                <div class="feature-block-content">
                    <?php if (!empty($Featured_image)) : ?>
                        <div class="feature-image">
                            <img src="<?= $Featured_image['url'] ?>" alt="">
                        </div>
                    <?php endif; ?>

                    <div class="feature-button">

                        <a href="<?=$button ?>"><button>View Details?</button></a>
                    </div>

                    <?php if (!empty($subheadline)) : ?>
                        <div class="feature-subheadline">
                            <?= $subheadline ?>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($colors)) : ?>
                    <div class="feature-colors">
                        <?php foreach ($colors as $color) : ?>
                            <div class="feature-color">
                                <div class="feature-color" style="background-color: <?= $color['color'] ?>"></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>

                    <?php if (!empty($code)) : ?>
                        <div class="feature-code">
                            Code - <?= $code  ?>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($price)) : ?>
                        <div class="feature-price">
                           $ <?= $price ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </section>


<?php if (!is_admin()) : ?>

<?php else: ?>
    featured product module
<?php endif; ?>