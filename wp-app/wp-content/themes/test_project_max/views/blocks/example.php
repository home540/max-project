<?php
/*
    Title: Example module
    Description: Example module
    Category: Page Headers
    Icon: admin-comments
    Keywords: example-module
    */
?>

<?php
$headline = get_field('headline');
$subheadline = get_field('subheadline');

?>

<?php if (!is_admin()) : ?>
    <section>
        <?php if (!empty($headline)) : ?>
            <div class="headline"><?= $headline ?></div>
        <?php endif; ?>
        <?php if (!empty($subheadline)) : ?>
            <div class="subheadline"><?= $subheadline ?></div>
        <?php endif; ?>
    </section>
<?php else: ?>
    example module
<?php endif; ?>