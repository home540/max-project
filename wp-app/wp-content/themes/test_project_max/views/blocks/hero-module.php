<?php
/*
    Title: Hero module
    Description: Hero module
    Category: Page Headers
    Icon: admin-comments
    Keywords: hero-module
    */
?>

<?php
$slides = get_field('slides');
$image_background = get_field('image_background');
?>

<?php if (!is_admin()) : ?>
    <?php if (!empty($slides)) : ?>
        <section>

            <?php foreach ($slides as $slide) : ?>
                <div class="Hero-module">
                    <div class="Hero-module-text">

                        <?php if (!empty($slide['caption'])) : ?>
                            <div class="caption">
                                <?= $slide['caption'] ?>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($slide['headline'])) : ?>
                            <div class="headline">
                                <?= $slide['headline'] ?>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($slide['subheadline'])) : ?>
                            <div class="subheadline">
                                <?= $slide['subheadline'] ?>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($slide['button'])) : ?>
                            <div class="button">
                                <a href="<?= $slide['button']['url'] ?>">
                                    <?= $slide['button']['title'] ?></a>
                            </div>
                        <?php endif; ?>
                    </div>

                    <?php if (!empty($slide['image'])) : ?>
                        <div class="image">
                            <img src="<?= $slide['image']['url'] ?>" alt="">
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($image_background)) : ?>
                        <div class="image_background">
                            <img src="<?= $image_background['url'] ?>" alt="">
                        </div>
                    <?php endif; ?>
                </div>

            <?php endforeach; ?>
        </section>
    <?php endif; ?>

<?php else: ?>
    Hero module
<?php endif; ?>