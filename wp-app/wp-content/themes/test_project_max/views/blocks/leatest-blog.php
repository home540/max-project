<?php
/*
    Title: leatest-blog
    Description: leatest-blog
    Category: Page Headers
    Icon: admin-comments
    Keywords: leatest-blog
    */
?>

<?php
$params = array(
    'post_type' => 'post', // тип постов - записи
    'numberposts' => 3, // получить 3 постов, можно также использовать posts_per_page
    'order' => 'DESC', // по убыванию (сначала - свежие посты)
);
$posts = get_posts($params);
$headline = get_field('headline');
?>


    <section>
        <div class="container">


            <?php if (!empty($headline)): ?>
                <div class="leatest-blog-headline">
                    <?= $headline ?>
                </div>
            <?php endif; ?>
            <div class="leatest-blog-container">
                <?php foreach ($posts as $post) : ?>

                    <?php

                    $title = $post->post_title;
                    $id = $post->ID;
                    $guid = $post->guid;
                    $subheadline = get_field('subheadline', $id);
                    $image = get_field('image', $id);
                    $date = $post->post_date;
                    $date = date_create($date);
                    $criterias = get_the_terms($id,'criteria_posts' );
                    ?>
                    <div class="leatest-blog-content">
                        <div class="leatest-blog-image">
                            <img src="" alt="">
                            <img src="<?= $image['url'] ?>" alt="">
                        </div>
                        <div class="leatest-blog-general">
                            <div class="leatest-blog-criterias">
                                <?php foreach ($criterias as $criteria):?>
                                    <?php ?>
                                    <?= $criteria->name ?>
                                <?php endforeach;?>
                            </div>
                            <div class="leatest-blog-date">
                                <?= date_format($date, 'j F, Y') ?>
                            </div>

                        </div>
                        <div class="leatest-blog-title">
                            <?= $title ?>
                        </div>
                        <div class="leatest-blog-subheadline">
                            <?= $subheadline ?>
                        </div>
                        <div class="leatest-blog-read-more">
                            <a href="<?= $guid ?>">Read More</a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

<?php if (!is_admin()) : ?>


<?php else: ?>
    Hero module
<?php endif; ?>