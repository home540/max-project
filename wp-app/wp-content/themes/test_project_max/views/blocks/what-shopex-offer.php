<?php
/*
    Title: What Shopex Offer! module
    Description: What Shopex Offer! module
    Category: Page Headers
    Icon: admin-comments
    Keywords: what-shopex-offer-module
    */
?>

<?php
$headline = get_field('headline');
$bloks = get_field('bloks');

?>

<?php if (!is_admin()) : ?>
    <section class="benefits">

        <div class="container">
            <div class="benefits-wrapper">
                <?php if (!empty($headline)): ?>
                    <div class="headline">
                        <?= $headline ?>
                    </div>
                <?php endif; ?>


                <?php if (!empty($bloks)) : ?>
                    <div class="benefist-wrapper-items">

                        <?php foreach ($bloks as $block) : ?>
                            <div class="benefist-item">

                                <?php if (!empty($block)): ?>
                                    <div class="module-2-image">
                                        <img src="<?= $block['image']['url'] ?>" alt="">
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($block)): ?>
                                    <div class="module-2-body-text-head">
                                        <?= $block ['headline'] ?>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($block)): ?>
                                    <div class="module-2-body-text-subhead">
                                        <?= $block ['subheadline'] ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>

                    </div>
                <?php endif; ?>

            </div>
        </div>

    </section>
<?php else: ?>
    What Shopex Offer! module
<?php endif; ?>