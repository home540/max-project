<?php
function mypilon_scripts_styles()
{
  wp_register_style('swiper_css', get_template_directory_uri() . '/assets/css/swiper.css', array(), '1.2', 'screen');
  wp_register_style('my_style', get_template_directory_uri() . '/assets/css/style.css', array(), '1.2', 'screen');

  wp_enqueue_style('swiper_css');
  wp_enqueue_style('my_style');

  wp_enqueue_script('jquery_js', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), '1.0', true);
  wp_enqueue_script('mask', get_template_directory_uri() . '/assets/js/jquery.mask.js', array(), '1.0', true);
  wp_enqueue_script('swiper_js', get_template_directory_uri() . '/assets/js/swiper.js', array(), '1.0', true);
  wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js', array(), '1.0', true);
  wp_localize_script('main', 'ajax', [
    'ajaxurl' => admin_url('admin-ajax.php'),
  ]);
}
add_action('wp_enqueue_scripts', 'mypilon_scripts_styles', 1);

if (function_exists('add_theme_support')) {
    add_theme_support('menus');
  }
  
  add_action('after_setup_theme', function () {
  
  
    register_nav_menus([
      'main_menu' => 'Меню',
    ]);
  
    add_theme_support(
      'custom-logo',
      array(
        'height'      => 500,
        'width'       => 500,
        'flex-height' => true,
      )
    );
  });

if (function_exists('acf_add_options_page')) {

  acf_add_options_page(array(
    'page_title'   => 'Theme General Settings',
    'menu_title'  => 'Глобальные настройки',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
}

add_filter('show_admin_bar', '__return_false');

function my_acf_block_render_callback( $block ) {

    // convert name ("acf/testimonial") into path friendly slug ("testimonial")
    $slug = str_replace( 'acf/', '', $block['name'] );

    // include a template part from within the "template-parts/block" folder
    if ( file_exists( get_theme_file_path( "/views/blocks/{$slug}.php" ) ) ) {
        include( get_theme_file_path( "/views/blocks/{$slug}.php" ) );
    }
}

add_action( 'acf/init', function () {
    if ( function_exists( 'acf_register_block' ) ) {
        // Look into views/blocks
        $dir = new DirectoryIterator( locate_template( "views/blocks/" ) );
        // Loop through found blocks
        foreach ( $dir as $fileinfo ) {
            if ( ! $fileinfo->isDot() ) {
                $slug = str_replace( '.php', '', $fileinfo->getFilename() );
                // Get infos from file
                $file_path    = locate_template( "views/blocks/${slug}.php" );
                $file_headers = get_file_data( $file_path, [
                    'title'       => 'Title',
                    'description' => 'Description',
                    'category'    => 'Category',
                    'icon'        => 'Icon',
                    'keywords'    => 'Keywords',
                ] );
                if ( empty( $file_headers['title'] ) ) {
                    die( _e( 'This block needs a title: ' . $file_path ) );
                }
                if ( empty( $file_headers['category'] ) ) {
                    die( _e( 'This block needs a category: ' . $file_path ) );
                }
                // Register a new block
                $datas = [
                    'name'            => $slug,
                    'title'           => $file_headers['title'],
                    'description'     => $file_headers['description'],
                    'category'        => $file_headers['category'],
                    'icon'            => $file_headers['icon'],
                    'keywords'        => explode( ' ', $file_headers['keywords'] ),
                    'className'       => '',
                    'render_callback' => 'my_acf_block_render_callback',
                ];
                acf_register_block( $datas );
            }
        }
    }
} );

add_action('init', 'create_new_custom_post_type');
function create_new_custom_post_type(){
    register_post_type('products', array(
        'labels'             => array(
            'name'               => 'Products', // Основное название типа записи
            'singular_name'      => 'Product', // отдельное название записи типа Book
            'add_new'            => 'Add Product',
            'add_new_item'       => 'Add Product',
            'edit_item'          => 'update Product',
            'new_item'           => 'New Product',
            'view_item'          => 'View product',
            'search_items'       => 'Search product',
            'not_found'          => 'Product not found',
            'not_found_in_trash' => 'Trash not found product',
            'parent_item_colon'  => '',
            'menu_name'          => 'Products'

        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'show_in_rest'       => true,
        'supports'           => array('title','editor','author','thumbnail','excerpt')
    ) );
}
add_action( 'init', 'create_taxonomy' );
function create_taxonomy(){
    register_taxonomy( 'categories_test', [ 'products' ], [
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => [
            'name'              => 'Категории',
            'singular_name'     => 'Категории',
            'search_items'      => 'Поиск по категориям',
            'all_items'         => 'Все категории',
            'view_item '        => 'Просмотри категории',
            'parent_item'       => 'Родительская категория',
            'parent_item_colon' => 'Родительская категория:',
            'edit_item'         => 'Редактирование категории',
            'update_item'       => 'Обновить категорию',
            'add_new_item'      => 'Добавить новую категорию',
            'new_item_name'     => 'Название новой категории',
            'menu_name'         => 'Категории',
        ],
        'description'           => '', // описание таксономии
        'public'                => true,
        'hierarchical'          => true,
        'rewrite'               => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities'          => array(),
        'meta_box_cb'           => null, // html метабокса. callback: post_categories_meta_box или post_tags_meta_box. false — метабокс отключен.
        'show_admin_column'     => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
        'show_in_rest'          => true, // добавить в REST API
        'rest_base'             => null, // $taxonomy
        // '_builtin'              => false,
        //'update_count_callback' => '_update_post_term_count',
    ] );
}
add_action( 'init', 'criteria_taxonomy' );
function criteria_taxonomy(){
    register_taxonomy( 'criteria_posts', [ 'post' ], [
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => [
            'name'              => 'Критерии',
            'singular_name'     => 'Критерии',
            'search_items'      => 'Поиск по критериям',
            'all_items'         => 'Все критерии',
            'view_item '        => 'Просмотри критерии',
            'parent_item'       => 'Родительская критерия',
            'parent_item_colon' => 'Родительская критерия:',
            'edit_item'         => 'Редактирование критерии',
            'update_item'       => 'Обновить критерии',
            'add_new_item'      => 'Добавить новые критерии',
            'new_item_name'     => 'Название новые критерии',
            'menu_name'         => 'Критерии',
        ],
        'description'           => '', // описание таксономии
        'public'                => true,
        'hierarchical'          => true,
        'rewrite'               => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities'          => array(),
        'meta_box_cb'           => null, // html метабокса. callback: post_categories_meta_box или post_tags_meta_box. false — метабокс отключен.
        'show_admin_column'     => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
        'show_in_rest'          => true, // добавить в REST API
        'rest_base'             => null, // $taxonomy
        // '_builtin'              => false,
        //'update_count_callback' => '_update_post_term_count',
    ] );
}



